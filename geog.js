$(function(){
  $('<div/>')
   .append('Your score: ')
   .append($('<span/>',{id:'correct-count',text:'0'}))
   .append(' of ')
   .append($('<span/>',{id:'attempt-count',text:'0'}))
   .appendTo('body')
  $('<div/>',{id:'question'})
    .appendTo('body');
  $.getJSON('http://progzoo.net/worldl.json',function(d){
    $('<button/>',{text:'Next Question',
     css:{display:'none'},
     id:'next-question',
     click:function(){
      var ri = Math.floor(d.length*Math.random());
      $('#question')
         .empty()
         .append($("<div/>",{
           id:'lead-in',
           text:"What is the capital of " + d[ri].name + "?"}))
         .append($('<div/>',{id:'distractor-list'}));
      var dl = [];
      for(var i=0;i<3;i++){
        var rd = Math.floor(d.length*Math.random());
        dl.push(d[rd].capital);
      }
      dl.push(d[ri].capital);
      dl.sort();
      for(var i=0;i<dl.length;i++){
        $('#distractor-list').append($('<button/>',{text:dl[i],'class':'dist'}));
      }
      $('button.dist').click(function(){
        var msg;
        if ($(this).text()===d[ri].capital){
          $('#correct-count').text(parseInt($('#correct-count').text())+1)
          msg = 'Well Done';
        }else{
          msg = 'No';
        }
        $('button.dist').prop('disabled',true);
        $('#attempt-count').text(parseInt($('#attempt-count').text())+1)
        $('<div/>',{id:'feedback',text:msg})
          .appendTo('#question')
          .fadeOut(2000,function(){
            $('#next-question').trigger('click');
          })
      });
    }}).appendTo('body');
    $('#next-question').trigger('click');
  });
});
